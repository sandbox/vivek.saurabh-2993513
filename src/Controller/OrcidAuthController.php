<?php

namespace Drupal\social_auth_orcid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\SocialAuthUserManager;
use Drupal\social_auth_orcid\OrcidAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Simple Orcid Connect module routes.
 */
class OrcidAuthController extends ControllerBase {

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  private $networkManager;

  /**
   * The user manager.
   *
   * @var \Drupal\social_auth\SocialAuthUserManager
   */
  private $userManager;

  /**
   * The Orcid authentication manager.
   *
   * @var \Drupal\social_auth_orcid\OrcidAuthManager
   */
  private $orcidManager;

  /**
   * Used to access GET parameters.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $request;

  /**
   * The Social Auth Data Handler.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  private $dataHandler;

  /**
   * OrcidAuthController constructor.
   *
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_orcid network plugin.
   * @param \Drupal\social_auth\SocialAuthUserManager $user_manager
   *   Manages user login/registration.
   * @param \Drupal\social_auth_orcid\OrcidAuthManager $orcid_manager
   *   Used to manage authentication methods.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_auth\SocialAuthDataHandler $social_auth_data_handler
   *   SocialAuthDataHandler object.
   */
  public function __construct(NetworkManager $network_manager,
                              SocialAuthUserManager $user_manager,
                              OrcidAuthManager $orcid_manager,
                              RequestStack $request,
                              SocialAuthDataHandler $social_auth_data_handler) {

    $this->networkManager = $network_manager;
    $this->userManager = $user_manager;
    $this->orcidManager = $orcid_manager;
    $this->request = $request;
    $this->dataHandler = $social_auth_data_handler;

    // Sets the plugin id.
    $this->userManager->setPluginId('social_auth_orcid');

    // Sets the session keys to nullify if user could not logged in.
    $this->userManager->setSessionKeysToNullify(['access_token', 'oauth2state']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_manager'),
      $container->get('social_auth_orcid.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('logger.factory')
    );
  }

  /**
   * Response for path 'user/login/orcid'.
   *
   * Redirects the user to Orcid for authentication.
   */
  public function redirectToOrcid() {
    /* @var \CILogon\OAuth2\Client\Provider\ORCID false $orcid */
    $orcid = $this->networkManager->createInstance('social_auth_orcid')->getSdk();

    // If orcid client could not be obtained.
    if (!$orcid) {
      drupal_set_message($this->t('Social Auth Orcid not configured properly. Contact site administrator.'), 'error');
      return $this->redirect('user.login');
    }

    // Destination parameter specified in url.
    $destination = $this->request->getCurrentRequest()->get('destination');
    // If destination parameter is set, save it.
    if ($destination) {
      $this->userManager->setDestination($destination);
    }

    // Orcid service was returned, inject it to $orcidManager.
    $this->orcidManager->setClient($orcid);

    // Generates the URL where the user will be redirected for Orcid login.
    // If the user did not have email permission granted on previous attempt,
    // we use the re-request URL requesting only the email address.
    $orcid_login_url = $this->orcidManager->getAuthorizationUrl();

    $state = $this->orcidManager->getState();

    $this->dataHandler->set('oauth2state', $state);

    return new TrustedRedirectResponse($orcid_login_url);
  }

  /**
   * Response for path 'user/login/orcid/callback'.
   *
   * Orcid returns the user here after user has authenticated in Orcid.
   */
  public function returnFromOrcid() {
    // Checks if user cancel login via Orcid.
    $error = $this->request->getCurrentRequest()->get('error');
    if ($error == 'access_denied') {
      drupal_set_message($this->t('You could not be authenticated.'), 'error');
      return $this->redirect('user.login');
    }

    /* @var \CILogon\OAuth2\Client\Provider\ORCID false $orcid */
    $orcid = $this->networkManager->createInstance('social_auth_orcid')->getSdk();

    // If orcid client could not be obtained.
    if (!$orcid) {
      drupal_set_message($this->t('Social Auth Orcid not configured properly. Contact site administrator.'), 'error');
      return $this->redirect('user.login');
    }

    $state = $this->dataHandler->get('oauth2state');

    // Retrieves $_GET['state'].
    $retrievedState = $this->request->getCurrentRequest()->query->get('state');
    
    if (empty($retrievedState) || ($retrievedState !== $state)) {
      $this->userManager->nullifySessionKeys();
      drupal_set_message($this->t('Orcid login failed. Invalid OAuth2 State.'), 'error');
      return $this->redirect('user.login');
    }

    // Saves access token to session.
    $this->dataHandler->set('access_token', $this->orcidManager->getAccessToken());

    $this->orcidManager->setClient($orcid)->authenticate();

    // Gets user's Orcid profile from Orcid API.
    if (!$orcid_profile = $this->orcidManager->getUserInfo()) {
      drupal_set_message($this->t('Orcid login failed, could not load Orcid profile. Contact site administrator.'), 'error');
      return $this->redirect('user.login');
    }
  
    // Gets user's email from the Orcid profile.
    if (!$email = $this->orcidManager->getUserInfo()->getEmail()) {
      drupal_set_message($this->t('Orcid login failed. This site requires permission to get your email address.'), 'error');
      return $this->redirect('user.login');
    }

    // Gets (or not) extra initial data.
    $data = $this->userManager->checkIfUserExists($orcid_profile->getId()) ? NULL : $this->orcidManager->getExtraDetails();

    // If user information could be retrieved.
    return $this->userManager->authenticateUser($orcid_profile->getFirstName(), $email, $orcid_profile->getId(), $this->orcidManager->getAccessToken(), $data);
  }

}
