<?php

namespace Drupal\social_auth_orcid\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Social Auth Orcid.
 */
class OrcidAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   Used to check if route exists.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   Used to check if path is valid and exists.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   Holds information about the current request.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteProviderInterface $route_provider, PathValidatorInterface $path_validator, RequestContext $request_context) {
    parent::__construct($config_factory, $route_provider, $path_validator);
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
    // Load the services required to construct this class.
      $container->get('config.factory'),
      $container->get('router.route_provider'),
      $container->get('path.validator'),
      $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_auth_orcid_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_orcid.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_auth_orcid.settings');

    $form['orcid_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Orcid App settings'),
      '#open' => TRUE,
      '#description' => $this->t('You need to first create a Orcid Sandbox account at <a href="@orcid-dev">@orcid-dev</a>', ['@orcid-dev' => 'https://sandbox.orcid.org/']),
    ];

    $form['orcid_settings']['app_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('app_id'),
      '#description' => $this->t('Copy the Client ID of your Orcid App here. This value can be found from your Developer Tool section.'),
    ];

    $form['orcid_settings']['app_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client secret'),
      '#default_value' => $config->get('app_secret'),
      '#description' => $this->t('Copy the Client secret of your Orcid App here. This value can be found from your Developer Tool section.'),
    ];
    
    $form['orcid_settings']['oauth_redirect_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Valid OAuth redirect URIs'),
      '#description' => $this->t('Copy this value to <em>Valid OAuth redirect URIs</em> field of your Orcid Account settings.'),
      '#default_value' => $GLOBALS['base_url'] . '/user/login/orcid/callback',
    ];

    $form['orcid_settings']['site_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Site URL'),
      '#description' => $this->t('Copy this value to <em>Your website URL</em> field of your Orcid Account settings.'),
      '#default_value' => $GLOBALS['base_url'],
    ];
  
    $form['orcid_settings']['account_type'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Account Type'),
      '#description' => $this->t('Member account or Sandbox Account.'),
      '#default_value' => 'sandbox',
      '#options' => array(
        'sandbox' => $this->t('Sandbox'),
        'member' => $this->t('Member'),
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('social_auth_orcid.settings')
      ->set('app_id', $values['app_id'])
      ->set('app_secret', $values['app_secret'])
      ->set('account_type', $values['account_type'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
