<?php

namespace Drupal\social_auth_orcid\Plugin\Network;

use Drupal\social_api\Plugin\NetworkInterface;

/**
 * Defines the Orcid Auth interface.
 */
interface OrcidAuthInterface extends NetworkInterface {}
